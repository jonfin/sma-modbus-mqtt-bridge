# SMA Modbus to MQTT Bridge

Allows you to read out the actual values of a standard SMA power converter and
publish it to a MQTT server.

## Examples

In `example` are some configuration example. Required is the `register.yaml`
with contains the translation of the Modbus register to a readable value.

For the Modbus devices there are two possiblities to configure the read out of
them: over a json file or over a yaml file.

## Starting

	pip3 install -r requirements.txt
	python3 modbus-mqtt.py -r register.yaml -c config.yaml

---

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

Copyright 2018 Jonas Fingerling
