# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Copyright 2018 Jonas Fingerling


import paho.mqtt.client as mqtt
from paho.mqtt.client import connack_string
from smamodbus import SmaModbus, SmaRegister
from ruamel.yaml import YAML
import asyncio
import logging

class Modbus:
    register = {}
    def __init__(self):
        self._client = mqtt.Client()
        self._client.on_connect = self.on_connect
        self._client.on_disconnect = self.on_disconnect
        self._client.on_message = self.on_message
        self.logger = logging.getLogger('modbus_mqtt.modbus')
    def load_registers(self, filebuffer):
        self.register = SmaRegister.loadRegisters(filebuffer)
    def load_config(self, filebuffer):
        self.logger.info('Loading yaml config')
        yaml = YAML(typ='safe')
        data = yaml.load(filebuffer)
        self._load_config_data(data)
    def load_config_ha(self, filebuffer):
        self.logger.info('Loading hassio config')
        import json
        data = json.load(filebuffer)
        for index, value in enumerate(data['sma']):
            data['sma'][index]['register'] = value['register'].split(',')
        self._load_config_data(data)
    def _load_config_data(self, data):
        self.logger.info('Loading config data')
        self.sma = SmaModbus.loadConfig(data['sma'], self.register)
        self._mqtt = {
                'host': data['mqtt'].get('host', 'localhost'),
                'port': data['mqtt'].get('port', 1883),
                'topic': data['mqtt'].get('topic', 'sma/')
                }
        if not self._mqtt['topic'].endswith('/'):
            self._mqtt['topic'] += '/'
        self.interval = data['interval']
    def on_connect(self, client, userdata, flags, rc):
        self.logger.info("Connection returned result: "+connack_string(rc))

    def on_disconnect(self, client, userdata, rc):
        if rc != 0:
            self.logger.warn("Unexpected disconnection.")

    def on_message(self, client, userdata, message):
        self.logger.info("Received message '" + str(message.payload) + "' on topic '"
            + message.topic + "' with QoS " + str(message.qos))
    def main_loop(self):
        self.logger.info('Starting Main Loop')
        self.event_loop = asyncio.get_event_loop()
        self.nexttime = {}
        for key in self.sma.keys():
            self.nexttime[key] = self.event_loop.time()
            self.event_loop.call_at(self.nexttime[key], self.callback, key)
        self._client.connect_async(self._mqtt['host'], self._mqtt['port'])
        self._client.loop_start()
        try:
            self.logger.info('Starting loop')
            self.event_loop.run_forever()
        except KeyboardInterrupt:
            pass
        finally:
            self.logger.info('closing event loop')
            self.event_loop.close()
            self._client.loop_stop()
    def publish(self, key):
        if not self.sma[key].connect():
            return
        results = self.sma[key].read_all()
        self.sma[key].disconnect()
        topic = self._mqtt['topic'] + key + '/'
        for reg, value in results.items():
            self._client.publish(topic+reg, value)

    def callback(self, key):
        self.publish(key)
        self.nexttime[key] += self.interval
        self.event_loop.call_at(self.nexttime[key], self.callback, key)

if __name__ == '__main__':
    # Load Logger
    logger = logging.getLogger('modbus_mqtt')

    # Add commandline arguments
    import argparse
    parser = argparse.ArgumentParser(description='SMA Modbus MQTT Bridge')
    parser.add_argument('-r', '--register', type=argparse.FileType('r'),
            help='YAML file with the register definitions', required=True)
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-c', '--config', type=argparse.FileType('r'),
            help='Configuration file')
    group.add_argument('--hassio', type=argparse.FileType('r'),
            help='Hassio Configuration file')
    parser.add_argument("-v", "--verbose", action="count", default=0,
        help="Verbosity (-v, -vv, etc)")
    args = parser.parse_args()

    # Set Logging verbosity
    logging.basicConfig(level=max(3 - args.verbose, 0) * 10)

    # Load registers
    m = Modbus()
    m.load_registers(args.register)
    args.register.close()
    # Load configuration
    if args.config:
        m.load_config(args.config)
        args.config.close()
    else:
        m.load_config_ha(args.hassio)
        args.hassio.close()
    # Start main loop
    m.main_loop()
