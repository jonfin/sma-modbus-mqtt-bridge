# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Copyright 2018 Jonas Fingerling

from pymodbus.client.sync import ModbusTcpClient as ModbusClient
from pymodbus.payload import BinaryPayloadDecoder as Decoder
from pymodbus.exceptions import ConnectionException
from ruamel.yaml import YAML
import struct
import logging

DEFAULT_HOST_PORT = 502

class SmaRegisterDecoder(Decoder):
    """Class for decoding the Modbus Payload

    Adds a 24bit decoder for the ENUM type of SMA Registers
    """
    def decode_24bit_uint(self):
        """ Decodes a 24 bit unsigned int from the buffer
        """
        self._pointer += 4
        fstring = 'I'
        handle = self._payload[self._pointer - 3:self._pointer]
        if self._byteorder == '<':
            handle += b'\0'
        else:
            handle = b'\0' + handle
        handle = self._unpack_words(fstring, handle)
        return struct.unpack("!"+fstring, handle)[0]

class SmaRegister:
    """Class describing a SMA register
    """
    _byteorder='>'
    _wordorder='>'
    _unit_length = {
            'S16': 1,
            'S32': 2,
            'STR32': 16,
            'U16': 1,
            'U32': 2,
            'U64': 4,
            }
    def __init__(self, name, address, unit_type, unit_format, unit='', enum={}, unit_id=0x03):
        self.name = name
        self.address = address
        self.unit_type = unit_type
        self.unit_format = unit_format
        self.unit_id = unit_id
        self.unit = unit
        self.enum = enum
        self.enum[0xfffffd] = 'NaN'
        self.logger = logging.getLogger('modbus_mqtt.SmaRegister')
    def parse(self, registers):
        """Function for parsing the payload and interpreting it
        """
        dec = SmaRegisterDecoder.fromRegisters(registers, byteorder=self._byteorder,
                wordorder=self._wordorder)
        if self.unit_type == 'S16':
            value = dec.decode_16bit_int()
            if abs(value) == 0x8000:
                value = 0
        elif self.unit_type == 'S32':
            value = dec.decode_32bit_int()
            if abs(value) == 0x80000000:
                value = 0
        elif self.unit_type == 'U16':
            value = dec.decode_16bit_uint()
            if value == 0xFFFF:
                value = 0
        elif self.unit_type == 'U32' and self.unit_format == 'ENUM':
            value = dec.decode_24bit_uint()
        elif self.unit_type == 'U32':
            value = dec.decode_32bit_uint()
            if value == 0xFFFFFFFF:
                value = 0
        elif self.unit_type == 'U64':
            value = dec.decode_64bit_uint()
            if value == 0xFFFFFFFFFFFFFFFF:
                value = 0
        elif self.unit_type == 'STR32':
            value = dec.decode_string(32)
            if value[0] == '\0':
                value = ''

        if self.unit_format == 'FIX0':
            pass
        elif self.unit_format == 'FIX1':
            value /= 10
        elif self.unit_format == 'FIX2':
            value /= 100
        elif self.unit_format == 'FIX3':
            value /= 1000
        elif self.unit_format == 'DT':
            value = datetime.fromtimestamp(value)
        elif self.unit_format == 'TM':
            value = datetime.fromtimestamp(value,tzinfo=tzinfo.tzname('UTC'))
        elif self.unit_format == 'ENUM':
            value = self.enum.get(value, value)
        return value
    def args(self):
        """Method for returning a directory with the configuration for the Modbus read out
        """
        return {
                'address':self.address,
                'count': self._unit_length.get(self.unit_type, 0),
                'unit':self.unit_id
                }
    def __str__(self):
        string = 'SMA Register: "{}", Address: {:5d}, Type: {:>5}, Format: {:>5}'
        return string.format(self.name, self.address, self.unit_type, self.unit_format)
    def loadRegisters(filebuffer):
        """Method to read the configuration file and returning the initialized registers
        """
        yaml = YAML(typ='safe')
        data = yaml.load(filebuffer)
        regs = {}
        for key, value in data.items():
            regs[key] = SmaRegister(
                    value['name'],
                    value['address'],
                    value['type'],
                    value['format'],
                    value.get('unit', ''),
                    value.get('enum', {}),
                    value.get('unitId', 0x03)
                    )
        return regs


class SmaModbus:
    """ Class for handling the connection per device
    """
    def __init__(self, host, port=DEFAULT_HOST_PORT):
        self._modbus = ModbusClient(host, port=port)
        self.register = {}
        self.logger = logging.getLogger('modbus_mqtt.SmaModbus')
    def connect(self):
        """Connecting to the Modbus Client
        """
        try:
            self._modbus.connect()
        except ConnectionException:
            self.logger.error('Could not connect to Modbus {}'.format(self._modbus))
            return False
        return True

    def disconnect(self):
        """Disconnect
        """
        self._modbus.close()
    def read(self, register):
        """ Read one register and returning the value
        """
        response = self._modbus.read_holding_registers(**register.args())
        if response.isError():
            self.logger.error('Could not read register "{}"'.format(register.name))
            return
        return register.parse(response.registers)
    def read_all(self):
        """Read all registers and returning the values as dict
        """
        res = {}
        try:
            for key, value in self.register.items():
                res[key] = self.read(value)
        except ConnectionException:
            self.logger.error('Could not connect to Modbus {}'.format(self._modbus))
        return res
    def loadConfig(data, register):
        """ Loading the Configuration (dict) and returning the initialized SmaModbus
        """
        sma = {}
        for value in data:
            key = value['name']
            sma[key] = SmaModbus(
                    value['host'],
                    value.get('port', DEFAULT_HOST_PORT)
                    )
            for reg in value.get('register', []):
                if reg in register:
                    sma[key].register[reg] = register[reg]
                else:
                    self.logger.error('Register "{}" not found'.format(reg))
        return sma

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='SMA Modbus Register Readout')
    parser.add_argument('-r', '--register', type=argparse.FileType('r'),
            help='YAML file with the register definitions')
    parser.add_argument('-c', '--config', type=argparse.FileType('r'),
            help='Configuration file')
    parser.add_argument("-v", "--verbose", action="count", default=0,
        help="Verbosity (-v, -vv, etc)")
    args = parser.parse_args()

    # Set Logging verbosity
    logging.basicConfig(level=max(3 - args.verbose, 0) * 10)

    regs = SmaRegister.loadRegisters(args.register)
    args.register.close()

    yaml = YAML(typ='safe')
    data = yaml.load(args.config)
    args.config.close()

    sma = SmaModbus.loadConfig(data['sma'], regs)
    for key, value in sma.items():
        value.connect()
        print(value.read_all())
        value.disconnect()
